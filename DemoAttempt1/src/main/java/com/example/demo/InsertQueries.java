package com.example.demo;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.example.demo.core.ImageInfo;
import com.example.demo.core.Product;
import com.example.demo.core.Unit;
import com.example.demo.repository.IdSequenceRepository;
import com.example.demo.repository.ProductRepository;

@Component
public class InsertQueries implements CommandLineRunner{
	
	@Autowired
	ProductRepository repository;
	
	@Autowired
	IdSequenceRepository idGen;
	

	@Override
	public void run(String... args) throws Exception {
		
		Unit u1 = new Unit();
		u1.setActive(true);
		u1.setQt("10");
		
		Unit u2 = new Unit();
		u2.setActive(true);
		u2.setQt("100");
		
		Unit u3 = new Unit();
		u3.setActive(true);
		u3.setQt("150");
		
		ImageInfo i1 = new ImageInfo();
		i1.setImageUrl("thumbnail.jpg");
		
		ImageInfo i2 = new ImageInfo();
		i2.setImageUrl("medium.jpg");
		
		ImageInfo i3 = new ImageInfo();
		i3.setImageUrl("https://5.imimg.com/data5/KR/DJ/MY-5579477/uden-300-tablets-500x500-500x500.jpg");
		i3.setPrimary(true);
		
		ImageInfo i4 = new ImageInfo();
		i4.setImageUrl("image2.jpg");
		ImageInfo i5 = new ImageInfo();
		i5.setImageUrl("image3.jpg");
		ImageInfo i6 = new ImageInfo();
		i6.setImageUrl("image4.jpg");
		
		Map<String, List<ImageInfo>> image = new HashMap<>();
		image.put("thumbnail", Arrays.asList(i1));
		image.put("medium", Arrays.asList(i2));
		image.put("images", Arrays.asList(i3, i4, i5, i6));
		
		Map<String, List<Unit>> quantity = new HashMap<>();
		quantity.put("numerical", Arrays.asList(u1, u2, u3));
		
		Product p1 = new Product();
		p1.setId(idGen.getNextSequenceId("product"));
		p1.setName("Crocin Advance");
		p1.setDescription("It is for fever.");
		p1.setIsActive(true);
		p1.setQuantity(quantity);
		p1.setCategory("tablets");
		p1.setImage(image);
		
		Product p2 = new Product();
		p2.setId(idGen.getNextSequenceId("product"));
		p2.setName("Paracetamol Fever tablet");
		p2.setDescription("It is for fever.");
		p2.setIsActive(true);
		p2.setQuantity(quantity);
		p2.setCategory("tablets");
		p2.setImage(image);
		
		Product p3 = new Product();
		p3.setId(idGen.getNextSequenceId("product"));
		p3.setName("Advanced feverish tablet");
		p3.setDescription("It is for fever.");
		p3.setIsActive(true);
		p3.setQuantity(quantity);
		p3.setCategory("tablets");
		p3.setImage(image);
		
		repository.saveAll(Arrays.asList(p1, p2, p3));
		
	}

}
