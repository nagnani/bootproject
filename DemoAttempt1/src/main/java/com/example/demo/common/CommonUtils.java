package com.example.demo.common;

import org.apache.commons.lang3.StringUtils;

public class CommonUtils {

	private CommonUtils() {
	}

	public static String addFullTextSearch(String search) {
		if (StringUtils.isNotBlank(search)) {
			String result = "";
			String[] splitSearch = search.split(" ");
			if (splitSearch.length <= 1) {
				result = splitSearch[0];
			} else {
				for (int i = 0; i < splitSearch.length; i++) {
					result += "(?=.*" + splitSearch[i].trim() + ")";
				}
				result += ".*";
			}
			return result;
		}
		return "";
	}

	public static String productNameToUrl(String name) {

		name = name.toLowerCase().replaceAll("[^a-zA-Z0-9]", "-");
		name = name.replace(" ", "-");
		name = name.replace("---", "-");
		name = name.replace("--", "-");
		if (name.endsWith("-"))
			name = StringUtils.chop(name);
		return name;
	}

}
