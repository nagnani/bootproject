package com.example.demo.context;

import java.util.Optional;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.context.ApplicationContext;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(value=AccessLevel.PROTECTED) @Setter 
public class DefaultBeanResolverStrategy implements BeanResolverStrategy {

	private final ApplicationContext applicationContext;

	public DefaultBeanResolverStrategy(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Override
	public <T> T find(Class<T> type) {
		String bNmArr[] = getApplicationContext().getBeanNamesForType(type);

		// 1st: consider single bean declared with or without qualifier of given type
		if (ArrayUtils.isNotEmpty(bNmArr) && bNmArr.length == 1)
			return getApplicationContext().getBean(bNmArr[0], type);

		// 2nd: if no bean OR multiple beans found by type, then use type's name as
		// qualifier
		return find(type, type.getSimpleName());
	}
	
	@Override
	public <T> T get(Class<T> type) {
		return Optional.ofNullable(find(type))
				.orElseThrow(() -> new RuntimeException("Bean of type " + type
						+ " Not Found"));
	}

	@Override
	public <T> T find(Class<T> type, String qualifier) {
		if(getApplicationContext().containsBean(qualifier)) 
			return getApplicationContext().getBean(qualifier, type);
		
		return getApplicationContext().containsBean(qualifier) ? getApplicationContext().getBean(qualifier, type) : null;
	}
}
