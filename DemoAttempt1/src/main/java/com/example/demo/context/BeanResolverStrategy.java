package com.example.demo.context;

public interface BeanResolverStrategy {

	<T> T find(Class<T> type);
	
	<T> T get(Class<T> type);
	
	<T> T find(Class<T> type, String qualifier);
	
}