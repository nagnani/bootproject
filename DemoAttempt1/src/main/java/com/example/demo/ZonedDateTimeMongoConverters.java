package com.example.demo;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.util.Assert;

import com.mongodb.DBObject;

public class ZonedDateTimeMongoConverters {

	public static final String K_DATE = "_date";
	public static final String K_ZONE = "_zone";
	
	@WritingConverter
	public static class ZDTSerializer implements Converter<ZonedDateTime, Bson> {
		@Override
		public Bson convert(ZonedDateTime z) {
			if(z==null)
				return null;
			
//			ZonedDateTime zUTC = z.withZoneSameInstant(ZoneOffset.UTC);
			ZonedDateTime zUTC = z.withZoneSameInstant(ZoneId.of("UTC"));
			
			Document dbObj = new Document();
			
			dbObj.put(K_DATE, Date.from(zUTC.toInstant()));
			dbObj.put(K_ZONE, z.getZone().getId());
			return dbObj;
		}
	}
	
	@ReadingConverter
	public static class ZDTDeserializer implements Converter<Bson, ZonedDateTime> {
		@Override
		public ZonedDateTime convert(Bson bson) {
			if(bson==null)
				return null;
			
			final Date date;
			final String zone;
			
			if(bson instanceof Document) {
				Document dbObj = (Document)bson;
				date = (Date)dbObj.get(K_DATE);
				zone = (String)dbObj.get(K_ZONE);
				
			} else if(bson instanceof DBObject) {
				DBObject dbObj = (DBObject)bson;
				date = (Date)dbObj.get(K_DATE);
				zone = (String)dbObj.get(K_ZONE);
				
			} else {
				throw new IllegalStateException("Unhandled database object type found: "+bson);
			}
			
			Assert.notNull(date, "Persisted entity for "+ZonedDateTime.class.getSimpleName()+" must not have null value for "+K_DATE);
			Assert.notNull(zone, "Persisted entity for "+ZonedDateTime.class.getSimpleName()+" must not have null value for "+K_ZONE);
			
			ZonedDateTime zUTC = ZonedDateTime.ofInstant(date.toInstant(), ZoneOffset.UTC);
			return zUTC.withZoneSameInstant(ZoneId.of(zone));
		}
	}

}