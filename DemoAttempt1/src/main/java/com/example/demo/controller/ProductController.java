package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.common.CommonUtils;
import com.example.demo.context.BeanResolverStrategy;
import com.example.demo.core.Product;
import com.example.demo.repository.IdSequenceRepository;
import com.example.demo.repository.ProductRepository;

@RestController
public class ProductController {
	
	private MongoTemplate mongoTemplate;
	private IdSequenceRepository idGenerator;
	private ProductRepository productRepository;
	
	public ProductController(BeanResolverStrategy beanResolver) {
		this.mongoTemplate = beanResolver.get(MongoTemplate.class);
		this.idGenerator = beanResolver.get(IdSequenceRepository.class);
		this.productRepository = beanResolver.get(ProductRepository.class);
	}
	

	@GetMapping("/products")
	public List<Product> getProducts() {

		return productRepository.findAll();
	}
	
	@GetMapping("/products/{id}")
	public Product getProduct(@PathVariable Long id) {

		Optional<Product> product = productRepository.findById(id);
		return product.isPresent()? product.get() : new Product();
	}
	
	@PutMapping("/products")
	public Product updateProduct(@RequestBody Product product) {
		
		productRepository.save(product);
		return product;
	}
	
	@PostMapping("/products")
	public Product insertProduct(@RequestBody Product product) {
		
		product.setId(idGenerator.getNextSequenceId("product"));
		product.setIsActive(true);
		productRepository.save(product);
		return product;
	}
	
	@DeleteMapping("/products/{id}")
	public Boolean deleteProduct(@PathVariable Long id) {
		
		Optional<Product> product = productRepository.findById(id);
		if(product.isPresent()) {
			Product retrievedProduct = product.get();
			retrievedProduct.setIsActive(false);
			productRepository.save(retrievedProduct);
			return true;
		}
		return false;
	}

	@GetMapping("/search")
	public List<Product> searchProducts(@RequestParam String query) {

		String queryBuild = CommonUtils.addFullTextSearch(query);
		
		Query que = new Query();
		que.addCriteria(Criteria.where("name").regex(queryBuild, "i").and("isActive").is(true));
		List<Product> products = mongoTemplate.find(que, Product.class);

		return products;
	}
	
	

}
