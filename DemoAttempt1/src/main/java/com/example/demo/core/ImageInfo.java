package com.example.demo.core;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImageInfo {
	
	private boolean isPrimary;
	
	private String imageUrl;

}
