package com.example.demo.core;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

import com.example.demo.common.CommonUtils;
import com.example.demo.core.entity.AbstractEntity;

import lombok.Getter;
import lombok.Setter;

@Document("product")
@Getter @Setter
public class Product extends AbstractEntity.IdLong{
	
	private static final long serialVersionUID = 1L;

	private String name;

	private String description;
	
	private String category;

	private Map<String, List<Unit>> quantity;
	
	private Map<String, List<ImageInfo>> image;
	
	public String getUrlName() {
		return CommonUtils.productNameToUrl(this.getName());
	}

}