package com.example.demo.core.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Document("sequence")
@Getter
@Setter
@ToString(callSuper = true)
public class DBSequence {
	
	@Id
	private String id;
	
	private long seq;

}
