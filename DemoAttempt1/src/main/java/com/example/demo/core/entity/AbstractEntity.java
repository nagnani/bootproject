package com.example.demo.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Persistable;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Nagarjuna Pechetti
 *
 */

@Getter
public abstract class AbstractEntity<ID extends Serializable> implements Serializable, Persistable<ID> {

	private static final long serialVersionUID = 1L;
	
	/*@Ignore
	private final String _class = this.getClass().getName();*/
	
	public static abstract class IdLong extends AbstractEntity<Long> {

		private static final long serialVersionUID = 1L;
		
		@Id @Getter @Setter
		private Long id;
	}
	
	public static abstract class IdString extends AbstractEntity<String> {
		
		private static final long serialVersionUID = 1L;
		
		@Id @Getter @Setter
		private String id;
	}
	
	public abstract ID getId();
	
	@Setter @CreatedBy
	private String user;
	
	@Setter @LastModifiedDate
	private LocalDateTime createdDate;
	
	@Setter @LastModifiedBy
	private String lastModifiedBy;
	
	@Setter @LastModifiedDate
	private LocalDateTime lastModifiedDate;
	
	@Setter private Boolean isActive;
	
	@Setter private long version;
	
	@Override
	public boolean isNew() {
		return this.lastModifiedDate == null;
	}

}
