package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.example.demo.repository.MongoIdSequenceRepository;

@Configuration
@EnableMongoRepositories(basePackages = "com.example.demo.repository")
@EnableJpaRepositories(basePackages = {"com.example.demo.repository"})
@EnableMongoAuditing
public class MongoConfig {

    private final List<Converter<?, ?>> converters = new ArrayList<Converter<?, ?>>();

    @Bean
    public MongoCustomConversions customConversions() {
      // ZDT Converters
        converters.add(new ZonedDateTimeMongoConverters.ZDTSerializer());
        converters.add(new ZonedDateTimeMongoConverters.ZDTDeserializer());
     // UTC Date Converters
        converters.add(new UTCDateMongoConverters.UTCDateSerializer());
        converters.add(new UTCDateMongoConverters.UTCDateDeserializer());
     // UTC LocalDateTime Converters
        converters.add(new UTCDateMongoConverters.UTCLocalDateTimeSerializer());
        converters.add(new UTCDateMongoConverters.UTCLocalDateTimeDeserializer());
     // UTC LocalDate Converters
        converters.add(new UTCDateMongoConverters.UTCLocalDateSerializer());
        converters.add(new UTCDateMongoConverters.UTCLocalDateDeserializer());
        return new MongoCustomConversions(converters);
    }

    @Bean
    MongoTransactionManager transactionManager(MongoDbFactory dbFactory) {
            return new MongoTransactionManager(dbFactory);
    }
    
    @Bean
	public MongoIdSequenceRepository mongoIdSequenceRepository(MongoOperations mongoOperations){
		return new MongoIdSequenceRepository(mongoOperations);
	}

}