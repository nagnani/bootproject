package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import com.example.demo.context.BeanResolverStrategy;
import com.example.demo.context.DefaultBeanResolverStrategy;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class DemoAttempt1Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoAttempt1Application.class, args);
	}
	
	@Bean	
	public BeanResolverStrategy defaultBeanResolver(ApplicationContext appCtx) {
		return new DefaultBeanResolverStrategy(appCtx);
	}

}
