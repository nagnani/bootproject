package com.example.demo.repository;

public interface IdSequenceRepository {

	public long getNextSequenceId(String key);
}
