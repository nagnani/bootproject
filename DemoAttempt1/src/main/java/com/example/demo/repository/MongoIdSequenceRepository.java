package com.example.demo.repository;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.example.demo.core.entity.DBSequence;

import lombok.Getter;

@Getter
public class MongoIdSequenceRepository implements IdSequenceRepository {

	MongoOperations mongoOperations;

	public MongoIdSequenceRepository(MongoOperations mongoOperations) {
		this.mongoOperations = mongoOperations;
	}

	@Override
	public long getNextSequenceId(String key) throws RuntimeException {
		// get sequence id
		Query query = new Query(Criteria.where("_id").is(key));

		// increase sequence id by 1
		Update update = new Update();
		update.inc("seq", 1);

		// return new increased id
		FindAndModifyOptions options = new FindAndModifyOptions();
		options.returnNew(true);
		options.upsert(true);

		DBSequence seqId = getMongoOperations().findAndModify(query, update, options, DBSequence.class, "sequence");

		if (seqId == null) {
			throw new RuntimeException("Unable to get sequence id for key : " + key);
		}

		return seqId.getSeq();
	}

}